/*
---------------------------------
		C++ CURSO B�SICO
			 ARRAYS
		POR: SALVADOR PAZOS
---------------------------------
*/

#include <iostream> 
using namespace std;

void main(){
	int arrayNumeros[5];
	// Un array puede verse como una tabla, en este caso tenemos una columna y 5 filas.
	// Es importante recordar que nuestro array comienza desde el 0.
	arrayNumeros[0] = 1;
	arrayNumeros[1] = 2;
	arrayNumeros[2] = 3;
	arrayNumeros[3] = 4;
	arrayNumeros[4] = 5;
	arrayNumeros[5] = 6; // Aqu� no podemos guardar nada debido a que nuestro array solamente tiene
						 // capacidad para almacenar 5 datos num�ricos enteros. Esto significa que
						 // el numero entre corchetes [] indica la cantidad de datos que vamos a guardar
						 // y no el �ndice. 

	// De igual forma, un array puede ser tanto int como float, char, double, bool, etc. 

	bool arrayBool[3];
	float arrayFloat[10];
	char arrayChar[30]; // Los array char pueden utilizarse para sustituir una variable String, debido
						// a que pueden almacenar tantos caracteres como le indiquemos. Tiene funcionalidad
						// cuando se quiere poner un n�mero fijo de caracteres, este caso podemos almacenar
						// 30. Se guarda de la siguiente manera: {h, o, l, a, , m, u, n, d, o } pero es
						// posible mostrarlo en la consola todo junto. 
	

	cout << "Escriba su nombre" << endl;
	//cin >> arrayChar;	// Es importante no poner espacios en el cin o no se almacenar� lo que se escriba despu�s. 
						// para solucionar esto tendremos que usar cin.getline(variable, tama�o, cuando va a terminar ('\n'))
	cin.getline(arrayChar, 30, '\n'); 
	cout << "Su nombre es: " << arrayChar << endl;

	// al llegar ac� habremos notado que nos sale un error en nuestro array de Char, eso se debe a que al declarar un arreglo o
	// variable, este se llena de basura. La forma m�s sencilla de arreglarlo es agregando: = ""  <-- en nuestra declaraci�n


	// As� como hay arreglos de una dimensi�n, tambi�n podemos encontrar otros de 2, 3, 4 e infinidad de dimensiones. De todas
	// maneras, en la pr�ctica lo m�s com�n es de usar de 1 y 2 dimensiones.

	int arrayInt2D[3][2]; // Este ser�a un array de dos dimensiones, podr�amos considerar que son 2 columnas y 3 filas o viceversa

	arrayInt2D[0][0] = 1;
	arrayInt2D[1][0] = 2;
	arrayInt2D[2][0] = 3;
	arrayInt2D[0][1] = 4;
	arrayInt2D[1][1] = 5;
	arrayInt2D[2][1] = 6;

	// Utilizando breakpoints podremos ver c�mo se guardaron los valores en nuestro array de dos dimensiones.

	system("pause");
}