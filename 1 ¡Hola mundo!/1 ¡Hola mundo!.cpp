/*
// ---------------------------------------- //
		   CURSO B�SICO DE C++
			  �HOLA MUNDO!
		   POR: SALVADOR PAZOS
// ---------------------------------------- //
*/
#include <iostream> // Una librer�a se puede ver de la siguiente forma: un mont�n de funciones guardadas en varios archivos
					// para facilitarnos la vida programando. Estas se cargan al iniciar al programa. 

using namespace std; // Por ahora nos debemos quedar con que esto nos ayuda a evitar escribir std:: antes de cada funci�n que
					 // vayamos a usar (cout y cin) 



void main()	// Esta es nuestra funci�n main, la cual se ejecutar� al dar inicio al programa. Aqu� va todo lo que vamos a querer hacer.
{			// recordemos que el compilador ejecuta el c�digo de izquierda a derecha y de arriba a abajo. 
			// en este caso void es el tipo de dato que nos va a devolver nuestra funci�n, como su nombre lo indica, el void no devuelve
			// nada
			// NOTA: El main SIEMPRE se ejecuta primero, sin importar si hay otras funciones antes. 

	cout << "Hola mundo" <<endl; // El cl�sico de la programaci�n. Esta funci�n cout nos permite mostrar un texto usando la consola.
								  // Pueden poner lo que quieran entre comillas. endl nos permite mandar un "enter" o salto de l�nea
								 // a la consola, para no tener todo pegado. 

	system("pause"); // Esto nos permite pausar la ejecuci�n de nuestro c�digo. Al quitarlo, el c�digo saldr� de la funci�n finalizando
					 // el programa

}