/*
-----------------------------------------
			C++ CURSO B�SICO
			  CONDICIONALES
			POR: SALVADOR PAZOS
-----------------------------------------
*/

#include <iostream>
using namespace std;

void main(){
	
	int numero;

	cout << "Elige un numero" << endl; // Pedimos al usuario que ingrese un numero entero
	cin >> numero;

	if (numero == 1){  // Un if es un tipo de condici�n la cu�l ejecutar� el c�digo dentro de las llaves
					   // en caso de cumplirse. En este caso, si el n�mero ingresado es igual a 1, 
					   // entrar� al "cout" mostrando el texto "Se eligio el numero 1". 

		cout << "Se eligio el numero 1" << endl;
	}

	system("pause");
	system("cls");

	cout << "Ingrese un numero menor a 10" << endl;
	cin >> numero;

	if (numero < 10){ // del mismo modo, podemos usar los operadores <, >, <=, >= para comparar.

		cout << "Se ingreso un numero correcto" << endl;

	}

	system("pause");
	system("cls");

	char caracter1;
	char caracter2;
	char caracter3;


	cout << "Ingrese un caracter" <<endl;
	cin >> caracter1;


	cout << "Ingrese un segundo caracter" << endl;
	cin >> caracter2;


	cout << "Ingrese un tercer caracter" << endl;
	cin >> caracter3;

	// NOTA: Hay una diferencia entre = y ==, la primera es una declaraci�n y la segunda es una comparaci�n.
	// = -> iguala
	// == -> compara

	if (caracter1 == caracter2 && caracter1 == caracter3) { //  or = ||    and = && 
														// Al usar ||, nuestro programa entrar� al if
														// en caso de que una condici�n se cumpla.

														// Al usar &&, el programa entra al if en caso
														// de que las dos comisiones se cumplan.

		cout << "El primer caracter es igual a los otros dos" << endl;
	}
	else {   // El else nos sirve para ejecutar una secci�n de c�digo en caso de que la condici�n
			 // del if no se haya cumplido

		cout << "El primer caracter es diferente a los demas D:" << endl;

	}

	

	// ------- PROGRAMA PARA DEVOLVER EL N�MERO MAYOR DE 3 N�MEROS INGRESADOS ---------
	int entero1, entero2, entero3;

	cout << "Ingrese 3 numeros enteros" << endl;
	cin >> entero1 >> entero2 >> entero3;
	
	if (entero1 > entero2){


		if (entero1 > entero3) 
		{
			cout << "El numero mayor es: " << entero1 << endl;
		}
		else 
		{ 
			cout << "El numero mayor es: " << entero3 << endl; 
		}


	}
	else if (entero2 > entero3) 
	{
		cout << "El numero mayor es: " << entero2 << endl;
	}
	else
	{
		cout << "El numero mayor es: " << entero3 << endl;
	}


	// PROGRAMA PARA SABER SI EL N�MERO INGRESADO ES PAR O IMPAR

	int myNumber;
	cout << "Ingrese un numero " << endl;
	cin >> myNumber;

	int par = myNumber % 2; // Esto nos devuelve el residuo de una divisi�n.

	if (par != 0)
		cout << "El numero: " << myNumber << " es impar" << endl;
	else
		cout << "El numero: " << myNumber << " es par" << endl;


	system("pause");
}