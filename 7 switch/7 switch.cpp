/*
-------------------------------------------------
				C++ CURSO B�SICO
					SWITCH
			  POR: SALVADOR PAZOS
-------------------------------------------------
*/

#include <iostream>
using namespace std;


void main(){
	int operador; // la variable que utilizaremos como operador para navegar en el switch

	do { // Podemos meter el switch en un ciclo do-while para repetirlo las veces que sean necesarias
		// (Tambi�n es posible usar el goto, el cual se declara goto + etiqueta y ponemos el nombre
		// de la etiqueta a donde vayamos a querer que salte nuestro programa)
		system("cls");

		cout << "Ingrese una opcion";
		cin >> operador;

		switch (operador) {	// Un switch es una especie de condicional en forma de men�, donde podremos 
							// separar los valores ingresados por casos


		case 1:	// Si operador == 1, el switch entrar� al caso 1 
			cout << "Caso 1" << endl;
			break; // un break hace que el ciclo termine en este punto, es bastante �til para
				   // utilizar switch. Intente quitar los break y vea qu� pasa al entrar al case 1

		case 2: {	// al declarar variables es recomendable utilizar {} en los case 
			int caso = 2;
			cout << "Caso 2" << endl;
			break;
		}

		default:
			cout << "Ningun caso" << endl;
			break;

		}

		// El Switch puede usarse con cualquier tipo de variable. Podemos utilizar char, bool, float, etc.
	} while (operador != 3);


	cout << "Saliento del programa... Presione ENTER.";
	system("pause>null"); // esto evita que nos mande el t�pico mensaje del system pause
}