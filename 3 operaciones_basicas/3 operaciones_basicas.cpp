/* 
  -----------------------------------------------------
				C++ CURSO B�SICO
				   OPERACIONES
				POR: SALVADOR PAZOS
  -----------------------------------------------------
*/


#include <iostream>
using namespace std;

/* Diferencia entre variables globales y locales
 Variables Globales: Se ejecutan todo el tiempo a partir de que el programa sea compilado hasta que se cierre
 Variables Locales: Se ejecutan cuando el programa llega a cierta l�nea de c�digo 

 EN POCAS PALABRAS: INTENTEN -NO- USAR VARIABLES GLOBALES
 */

//Operaciones b�sicas con int y float.
//Pruebas de operaciones con char y bool.  
void main() {
	int a = 3;			// Hay palabras reservadas en C++ que est�n definidas para ejecutar una acci�n o establecer un par�metro, 
	float b = 4;		// por eso mismo no podemos llamar a una variable de cierta manera, como por ejemplo: char int = 'A'.
	char caracter = '2';  // �Qu� pasa si quito los ' '?  R= Me da el valor en c�digo ASCII
	char caracterNum = 80;

	int variable = a / b;    // Nuestra variable 'variable' tipo int ser� = a la divisi�n de 'a' sobre 'b' 

	float variableFloat = a / b;  // Lo mismo pero con un float 


	a = 65;		// Puedo cambiar el valor de las variables sin tener que declararlas, y las operaciones anteriores no se ven afectadas
	b = 10;		// ya que el programa corre de arriba hacia abajo

	char suma = b + a;  // �por qu� podemos hacer esto? R= Porque seguimos trabajando con c�digo ASCII

	char cadena1[5] = "hola";	// a pesar de que "hola" solo tiene 4 letras, el programa nos obliga a poner un array de 5, esto se 
								// debe a que al final de cada array de char debe haber un \0 que indique el fin de la secuencia.
								// N�tese c�mo un char se guarda de esta {'m','a','n','e','r','a'} utilizando breakpoints

	char cadena2[7] = "amigos";	 // No hace falta contar cu�ntas letras vamos a utilizar en cada variable tipo char, podemos asignar, por ejemplo
								 // un char de [20] sin ning�n problema dependiendo de cu�ntos caracteres m�ximos vamos a querer.
								 // tip: si se quiere declarar de manera exacta la cantidad de espacios necesarios, C++ nos dice al poner caracteres
								 //      dem�s ;)


	char cadena3[5] = { 'A', 'b', 'c' }; // otra forma de declarar char totalmente v�lida ya que es realmente como el sistema lo guarda. 
	// no nos deja sumar variable char

	string myString1 = "Hola";	 // String se acopla al tama�o del array que est�s utilizando, podr�amos decir que de cierta forma se estira.
	string myString2 = "Clase";
	string myString3 = myString1 + myString2;		// La magia de los string, concatenamos con una suma. 

	cout << "variable = " << variable << endl;

	cout << "variableFloat = " << variableFloat << endl;

	cout << "caracter = " << caracter << endl;

	cout << "caracterNum = " << caracterNum << endl;	// C�digo ASCII, solo lo tienen que googlear 

	cout << "suma = " << suma << endl;

	cout << myString1 + myString2 << endl;
	cout << myString3 << endl;

	system("pause");
}