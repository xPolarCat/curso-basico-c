/*
// ---------------------------------------- //
		    CURSO B�SICO DE C++
		    TIPOS DE VARIABLES
		    POR: SALVADOR PAZOS
// ---------------------------------------- //
*/

/*Include
Define
Namespace
Variables
Estructuras
Declarar Funciones*/

#include <iostream>
#include <string>
using namespace std;

void main() {
	/*Computing the size of some C++ inbuilt variable types
		Size of bool: 1
		Size of char: 1
		Size of unsigned short int: 2
		Size of short int: 2
		Size of unsigned long int: 4
		Size of long: 4
		Size of int: 4
		Size of unsigned long long: 8
		Size of long long: 8
		Size of unsigned int: 4
		Size of float: 4
		Size of double: 8
		The output changes with compiler, hardware and OS*/


		// tipos de variables m�s importantes que vamos a usar siempre: bool, int, float, char, string*
	bool booleano;
	int numero;
	float numeroDecimal;
	char caracter;
	string secuenciaCaracteres = "\0";

	booleano = true; // puede almacenar true y false				// sizeof(variable) nos devuelve el peso en bytes de la variable ingresada
	cout << "Bool: " << booleano << "\t Pesa: " << sizeof(booleano) << endl << endl;  // \t nos da un tab
	
	numero = 5; // almacena numeros del -123 hasta 123 sin decimales
	cout << "Int:  " << numero << "\t Pesa: " << sizeof(numero) << endl << endl; 

	numeroDecimal = 50.23382; // almacena hasta 7 decimales
	cout << "Float: " << numeroDecimal << "\t Pesa: " << sizeof(numeroDecimal) << endl << endl;  // float e int mismo tama�o: https://stackoverflow.com/questions/8746443/float-and-int-both-4-bytes-how-come/8746839

	system("pause>null");
}