/*
-------------------------------------------------
				C++ CURSO B�SICO
				   FUNCIONES
			   POR: SALVADOR PAZOS
-------------------------------------------------
*/

/* 
Include
Define
Namespace
Variables
Estructuras
Declarar Funciones 
*/ 

#include <iostream>
#include <string>
using namespace std;


// Las funciones se declaran justo antes de nuestra funci�n main. A estas declaraciones
// se les llama: Prototipo de funciones. 

void holaMundo();
int funcionInt();
bool funcionBool(int Mes);
string funcionString();

// NOTA: Las funciones no ocupan espacio en memoria hasta que son llamadas.

void main(){  // Hasta ahora, esta ha sido la �nica funci�n que hemos estado utilizando. 
		

	holaMundo(); // mando a llamar la funci�n

	int mes;
	cout << "Ingrese un numero" << endl;
	cin >> mes;

	bool validar = funcionBool(mes);

	if (validar)
		cout << "El numero: " << mes << " si corresponde a un mes del anio" << endl;
	else
		cout << "El numero: " << mes << " no corresponde a un mes del anio" << endl;


	system("pause");
}

void holaMundo(){

	cout << "Hola Mundo :D" << endl;

	system("pause");
	system("cls");

	return;
}

bool funcionBool(int mes){

	if (mes <= 12 && mes >= 1)
		return true; // regresamos un valor TRUE si no se cumple la condici�n
	

	return false;	// regresamos un valor FALSE si no se cumple la condici�n
}