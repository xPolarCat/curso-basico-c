#include <iostream>
#include <string>
#include <sstream> 
using namespace std;

int cantidadProductos;
int carritoID[100], carritoCantidad[100];
int indiceCarrito = 0;

struct Tienda {
	string nombre;
	float precio;
}producto[100];

void tienda();

void limpiarCarrito();
bool revisarCarrito(int);
void verCarrito();

void agregar();


void main()
{	
	tienda();
	limpiarCarrito();
	string opcionTemporal;
	int opcion;

	do {
		system("cls");
		cout << "----------Menu----------" << endl << endl;
		cout << "1.- Agregar producto" << endl;
		cout << "2.- Eliminar producto" << endl;
		cout << "3.- Modificar producto" << endl;
		cout << "4.- Ver carrito" << endl;
		cout << "5.- Imprimir Ticket" << endl;
		cout << "6.- Salir" << endl;
		getline(cin, opcionTemporal);

		stringstream intValue(opcionTemporal);
		intValue >> opcion;		

		system("cls");
		switch (opcion)
		{

			case 1:
				agregar();
				break;

			case 2:
				cout << "Funcion pendiente..." << endl;
				break;

			case 3:
				cout << "Funcion pendiente..." << endl;
				break;

			case 4:
				verCarrito();
				break;

			case 5:
				break;

			case 6:
				cout << "Saliendo del programa..." << endl;
				break;

			default:
				cout << "Opcion incorrecta, intenta de nuevo" << endl;
				break;

		}

		system("pause");

	} while (opcion != 6);




}


void tienda() {

	producto[0].nombre = "Americano";
	producto[0].precio = 27;

	producto[1].nombre = "Expreso";
	producto[1].precio = 32.5;

	producto[2].nombre = "Moka";
	producto[2].precio = 39;

	producto[3].nombre = "Cappuccino";
	producto[3].precio = 45;

	producto[4].nombre = "Latte";
	producto[4].precio = 35.5;

	cantidadProductos = 5;
}

void limpiarCarrito(){
	int capacidad = sizeof(carritoID)/4;
	
	for (int i = 0; i < capacidad; i++) 
	{
		carritoID[i] = 0;
		carritoCantidad[i] = 0;
	}

}

bool revisarCarrito(int seleccion) {

	int capacidad = sizeof(carritoID) / 4;

	for (int i = 0; i < capacidad; i++)
	{
		if (seleccion == carritoID[i])
			return true;
	}

	return false;
}

void verCarrito() {

	int capacidad = sizeof(carritoID) / 4;

	for (int i = 0; i < indiceCarrito; i++)
	{	
		cout << producto[carritoID[i]].nombre << "  ";
		cout << "Cantidad: " << carritoCantidad[i] << endl;
	}   
	
}

void agregar() {

	int seleccion, cantidad;

	cout << "------Lista de productos------" << endl << endl;
	for (int i = 0; i < cantidadProductos; i++)
	{
		cout << i << ".- " << producto[i].nombre;
		cout << "  $" << producto[i].precio << endl;
	}

	cout << endl << endl << "Elija un producto segun su ID" << endl;
	cin >> seleccion;
	fflush(stdin);
	cout << "Elija la cantidad a comprar" << endl;
	cin >> cantidad;
	fflush(stdin);
	getchar(); // GRACIAS CERTI (Poderosa funci�n anti bugs)

	if (revisarCarrito(seleccion)) // si ya se hab�a ingresado este producto antes, sumo la cantidad
	{
		int capacidad = sizeof(carritoID) / 4;
		for (int i = 0; i < capacidad; i++)
		{
			if (seleccion == carritoID[i])
			{
				carritoCantidad[i] += cantidad;
 			}
		}
	}
	else
	{
		carritoID[indiceCarrito] = seleccion;
		carritoCantidad[indiceCarrito] = cantidad;
		indiceCarrito++;
	}

}
