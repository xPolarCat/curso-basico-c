/*
------------------------------------------------
				C++ CURSO B�SICO
					CICLOS
			   POR: SALVADOR PAZOS
------------------------------------------------
*/

#include <iostream>
using namespace std;


void main() {

	// Un ciclo es una secci�n de c�digo que se va a repetir mientras una condici�n sea cumplida
	// En C++ tenemos tres tipos de ciclos: For, While y Do-While

	for (int i = 0; i < 5; i++) // El ciclo for contiene 3 par�metros:
	{							// -Una variable que ir� aumentando o disminuyendo
								// -Una condici�n a cumplir
								// -El incremento o decremento de nuestra variable
		cout << "Numero: " << i << endl;
		
	}	// El programa va a imprimir el valor de i cada vez que el ciclo vuelva a ejecutarse,
		// de esta forma vemos c�mo nuestra variable va aumentando +1 cada vez que llega al cout.
		



	int i = 0;
	while (i < 6) {	// En el ciclo while, entraremos a esta secci�n del c�digo y la repetiremos
					// siempre y cuando nuestra condici�n se cumpla.

		cout << "Numero: " << i << endl;
		i++; // Es importante aumentar la variable cada vez que se llegue a este punto o nos encontraremos
	}	     // en un ciclo infinito
	



	int numero;		// Un ciclo Do-While entrar� a esta secci�n de c�digo al menos 1 vez, y no saldr� 
	do {			// de aqu� mientras la condici�n sea cumplida.

		cout << "Ingrese un numero diferente de 3" << endl;	// Pedimos al usuario ingresar un
		cin >> numero;										// numero diferente de 3

		if (numero != 3) // En caso de que el numero ingresado sea diferente de 3
			cout << "Numero correcto" << endl; // imprimimos "Numero correcto"
		else // de lo contrario
			cout << "Numero incorrecto" << endl; // imprimimos "Numero incorrecto"

	} while (numero == 3); // Condici�n del Do-While la cual har� que el ciclo se repita
						   // MIENTRAS nuestro numero sea = a 3



	system("pause");
}

