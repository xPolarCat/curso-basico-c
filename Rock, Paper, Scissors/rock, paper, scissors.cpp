/*
	JUEGO DE PIEDRA, PAPEL O TIJERA EN CONSOLA
			POR: SALVADOR PAZOS
*/
#include<iostream>
#include <sstream>
#include<time.h>
using namespace std;

void juego();
void imprimirEleccion(int, int);

void main(){

	string operadorTemp;
	int operador;

	do 
	{	
		system("cls");

		cout << "---------- Piedra, Papel o Tijera ----------" << endl;
		cout << "Escriba la opcion deseada y presione enter: " << endl;
		cout << "1.- Jugar" << endl;
		cout << "2.- Salir" << endl;
		getline(cin, operadorTemp);	// ingreso una variable string para que el programa no crashee 
		fflush(stdin); // limpieza del buffer 

		stringstream intValue(operadorTemp);  // funci�n que guarda el valor de tempOperador como INT en la variable intValue 
		intValue >> operador;

		system("cls");

		switch (operador) 
		{

		case 1:
			juego();
			break;


		case 2:
			cout << "Saliendo del programa... Presione ENTER";
			break;

		default:
			cout << "Opcion incorrecta, ingrese una opcion valida" << endl;
			system("pause");
			break;
		}
		
	} while (operador != 2);

	system("PAUSE>NULL");

	return;
}


void juego(){

	string jugadorTemp;
	int jugador, computadora;

	salto:
	system("cls");

	cout << "Elija una opcion" << endl;
	cout << "1.- Piedra" << endl;
	cout << "2.- Papel" << endl;
	cout << "3.- Tijera" << endl;
	getline(cin, jugadorTemp);
	fflush(stdin);

	stringstream intValue(jugadorTemp);
	intValue >> jugador;

	system("cls");

	if (jugador < 1 || jugador > 3)
	{
		cout << "Opcion incorrecta. Vuelve a intentarlo..." << endl;
		system("PAUSE");
		goto salto;
	}

	srand(time(NULL));
	computadora = rand() % 3 + 1;

	imprimirEleccion(jugador, computadora);

	cout << endl;

	if ((jugador == 1 && computadora == 3) ||
		(jugador == 2 && computadora == 1) ||
		(jugador == 3 && computadora == 2)) 
	{
		cout << "�Felicidades, ganaste!" << endl;
	}
	else if (jugador == computadora) 
	{
		cout << "�Empate!" << endl;
	}
	else 
	{
		cout << "�Perdiste! Intenta de nuevo." << endl;
	}

	cout << endl;

	system("pause");
	return;
}



void imprimirEleccion(int jugador, int computadora)
{
	switch (jugador) {
	case 1:
		cout << "El jugador eligio: Piedra" << endl;
		break;

	case 2:
		cout << "El jugador eligio: Papel" << endl;
		break;

	case 3:
		cout << "El jugador eligio: Tijera" << endl;
		break;
	}

	switch (computadora) {
	case 1:
		cout << "La computadora eligio: Piedra" << endl;
		break;

	case 2:
		cout << "La computadora eligio: Papel" << endl;
		break;

	case 3:
		cout << "La computadora eligio: Tijera" << endl;
		break;
	}

	return;
}