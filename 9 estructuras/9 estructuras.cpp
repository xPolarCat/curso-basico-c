/*
-------------------------------------------------
				C++ CURSO B�SICO
				   ESTRUCTURAS
			   POR: SALVADOR PAZOS
-------------------------------------------------
*/

#include <iostream>
#include <string>
using namespace std;


struct miEstructura { // Una estructura puede verse como una tabla de datos que tiene un �ndice
					 // el cual declaramos en forma de arreglo al final de la misma. 
	string nombre;
	int numero;
	bool vivo;

}dato[40];


void main(){


	for (int i = 0; i < 6; i++) {  // llenamos los primeros 6 �ndices de la estructura con datos
		cout << "Ingrese un nombre: " << endl;
		cin >> dato[i].nombre;	// n�tese c�mo uso el arreglo que declar� al final de
								// la estructura, antes del ; para seleccionar en qu�
								// posici�n voy a guardar los datos.

		cout << "Ingrese un numero: " << endl;
		cin >> dato[i].numero;

		cout << "Ingrese si esta vivo: " << endl;
		cin >> dato[i].vivo;

		system("cls");
	}

	for (int i = 0; i < 6; i++) {	// y los mostramos
		cout << dato[i].nombre << endl;
		cout << "   Num: " << dato[i].numero << endl;
		cout << "  Vivo: " << dato[i].vivo << endl;
	}

	system("pause>null");
}